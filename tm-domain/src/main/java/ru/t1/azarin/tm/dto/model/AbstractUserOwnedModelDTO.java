package ru.t1.azarin.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnedModelDTO extends AbstractModelDTO {

    @NotNull
    @Column(nullable = false, name = "user_id")
    private String userId;

}
