package ru.t1.azarin.tm.dto.request.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class UserViewProfileRequest extends AbstractUserRequest {

    public UserViewProfileRequest(@Nullable final String token) {
        super(token);
    }

}
