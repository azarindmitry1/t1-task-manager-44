package ru.t1.azarin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.dto.model.AbstractModelDTO;

public interface IRepositoryDTO<M extends AbstractModelDTO> {

    void add(@NotNull M model);

    void remove(@NotNull M model);

    void update(@NotNull M model);

}
