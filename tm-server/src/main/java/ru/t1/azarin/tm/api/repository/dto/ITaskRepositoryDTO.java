package ru.t1.azarin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.TaskDTO;
import ru.t1.azarin.tm.enumerated.Sort;

import java.util.List;

public interface ITaskRepositoryDTO extends IUserOwnedRepositoryDTO<TaskDTO> {

    @Nullable
    List<TaskDTO> findAll(@NotNull String userId, @NotNull Sort sort);

    @Nullable
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}