package ru.t1.azarin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.service.dto.*;
import ru.t1.azarin.tm.api.service.model.*;

public interface IServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectServiceDTO getProjectServiceDTO();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IProjectTaskServiceDTO getProjectTaskServiceDTO();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    ITaskServiceDTO getTaskServiceDTO();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IUserServiceDTO getUserServiceDTO();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    ISessionServiceDTO getSessionServiceDTO();

}
