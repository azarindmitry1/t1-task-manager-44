package ru.t1.azarin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.repository.dto.IRepositoryDTO;
import ru.t1.azarin.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;

public abstract class AbstractRepositoryDTO<M extends AbstractModelDTO> implements IRepositoryDTO<M> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractRepositoryDTO(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

}
