package ru.t1.azarin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.t1.azarin.tm.dto.model.ProjectDTO;
import ru.t1.azarin.tm.enumerated.Sort;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectRepositoryDTO extends AbstractUserOwnedRepositoryDTO<ProjectDTO> implements IProjectRepositoryDTO {

    public ProjectRepositoryDTO(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM ProjectDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId, @NotNull final Sort sort) {
        @NotNull final String jpql = String.format("SELECT m FROM ProjectDTO m WHERE m.userId = :userId ORDER_BY m.%s",
                getSortColumn(sort.getComparator()));
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "DELETE FROM ProjectDTO m WHERE m.userId = :userId AND m.id = :id";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

}
