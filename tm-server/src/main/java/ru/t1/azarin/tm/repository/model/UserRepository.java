package ru.t1.azarin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.api.repository.model.IUserRepository;
import ru.t1.azarin.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        @Nullable final List<User> users = findAll();
        if (users == null) return;
        for (@NotNull final User user : users) entityManager.remove(user);
    }

    @Nullable
    @Override
    public List<User> findAll() {
        @NotNull final String jpql = "SELECT m FROM User";
        return entityManager.createQuery(jpql, User.class)
                .getResultList();
    }

    @Nullable
    @Override
    public User findOneById(@NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.id = :id";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.login = :login";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.email = :email";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(findOneById(id));
    }

}


