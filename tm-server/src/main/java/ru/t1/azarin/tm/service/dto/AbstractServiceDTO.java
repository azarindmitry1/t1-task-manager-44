package ru.t1.azarin.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.repository.dto.IRepositoryDTO;
import ru.t1.azarin.tm.api.service.IConnectionService;
import ru.t1.azarin.tm.api.service.dto.IServiceDTO;
import ru.t1.azarin.tm.dto.model.AbstractModelDTO;

public abstract class AbstractServiceDTO<M extends AbstractModelDTO, R extends IRepositoryDTO<M>> implements IServiceDTO<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractServiceDTO(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
