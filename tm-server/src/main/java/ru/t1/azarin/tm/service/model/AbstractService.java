package ru.t1.azarin.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.api.repository.model.IRepository;
import ru.t1.azarin.tm.api.service.IConnectionService;
import ru.t1.azarin.tm.api.service.model.IService;
import ru.t1.azarin.tm.model.AbstractModel;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
